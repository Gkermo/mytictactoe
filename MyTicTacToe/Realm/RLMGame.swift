//
//  RLMGame.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import RealmSwift

class RLMGame: Object
{    
    dynamic var mode = ""
    dynamic var difficulty = ""
    dynamic var result = ""
    dynamic var firstMove = NSNumber(integer: 99)
    dynamic var firstPlayer = ""
    dynamic var date = NSDate()
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
