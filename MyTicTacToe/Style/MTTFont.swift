//
//  MTTFont.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

enum AppFontType
{
    case Normal
    case Bold
}


class MTTFont: UIFont {

}

extension UIFont {
    
    class func appFontOfType(type: AppFontType, size: CGFloat) -> UIFont
    {
        switch type
        {
        case .Normal:
            return UIFont(name: "HelveticaNeue", size: size)!
        case .Bold:
            return UIFont(name: "HelveticaNeue-Bold", size: size)!
        }
    }
}