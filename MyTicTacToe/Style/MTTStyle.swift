//
//  MTTStyle.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 04/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

// Here are mostly defined the colors of the app
// - main color : game board, navigation bar background
// - second color : labels game view, headers table views
// - third color : backgrounds
// - fourth color : labels navigation bar, labels table view headers
// - fifth color : table view cells labels

import UIKit

class MTTStyle: NSObject {
    
    // Default style
    class func setDefaultStyleIfNoStyleIsSet()
    {
        let defaults = NSUserDefaults()
        if (defaults.objectForKey(KEY_MAIN_COLOR) == nil)
        {
            MTTStyle.setMainColor(cyanColor)
            MTTStyle.setSecondColor(blueColor)
            MTTStyle.setThirdColor(lightBlueColor)
            MTTStyle.setFourthColor(whiteColor)
            MTTStyle.setFifthColor(blackColor)
        }
    }
    
    // Colors
    // Getters
    class func getMainColor() -> UIColor
    {
        return getColorForKey(KEY_MAIN_COLOR)
    }
    
    class func getSecondColor() -> UIColor
    {
        return getColorForKey(KEY_SECOND_COLOR)
    }
    
    class func getThirdColor() -> UIColor
    {
        return getColorForKey(KEY_THIRD_COLOR)
    }
    
    class func getFourthColor() -> UIColor
    {
        return getColorForKey(KEY_FOURTH_COLOR)
    }
    
    class func getFifthColor() -> UIColor
    {
        return getColorForKey(KEY_FIFTH_COLOR)
    }
    
    // Setters
    class func setMainColor(color: UIColor)
    {
        setColorForKey(color, key: KEY_MAIN_COLOR)
    }
    
    class func setSecondColor(color: UIColor)
    {
        setColorForKey(color, key: KEY_SECOND_COLOR)
    }
    
    class func setThirdColor(color: UIColor)
    {
        setColorForKey(color, key: KEY_THIRD_COLOR)
    }
    
    class func setFourthColor(color: UIColor)
    {
        setColorForKey(color, key: KEY_FOURTH_COLOR)
    }
    
    class func setFifthColor(color: UIColor)
    {
        setColorForKey(color, key: KEY_FIFTH_COLOR)
    }
    
    // Helpers
    class func getColorForKey(key: String) -> UIColor
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let colorString = defaults.objectForKey(key)
        {
            if colorString is String
            {
                return MTTUtils.colorFromString(colorString as! String)
            }
        }
        
        return UIColor.whiteColor()
    }
    
    class func setColorForKey(color: UIColor, key: String)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(MTTUtils.stringFromUIColor(color), forKey: key)
        defaults.synchronize()
    }
}
