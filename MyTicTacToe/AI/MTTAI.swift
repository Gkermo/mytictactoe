//
//  MTTAI.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 25/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTAI: NSObject {
    
    class func play(collectionView: UICollectionView, player1Array: Array<NSNumber>, player2Array: Array<NSNumber>) -> NSIndexPath?
    {
        let difficulty = MTTUtils.getCurrentDifficulty()
        
        let chanceToPlayTheRightMove : Int32 = difficulty == DIFFICULTY_HARD ? 1 : 2
        
        var totalArray = Array<NSNumber>()
        totalArray.appendContentsOf(player1Array)
        totalArray.appendContentsOf(player2Array)
        
        // Win
        if let nextIndexToPlay = indexToWin(player2Array, totalArray: totalArray)
        {
            return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
        }
        // Block
        if rand()%chanceToPlayTheRightMove == 0
        {
            if let nextIndexToPlay = indexToWin(player1Array, totalArray: totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
            
            // Play on center
            if let nextIndexToPlay = playOnCenter(totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
        }
        
        if difficulty == DIFFICULTY_HARD
        {
            // Play against specific move
            if let nextIndexToPlay = indexToAvoidKillMove(player1Array, player2Array: player2Array, totalArray: totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
            
            // Fork
            if let nextIndexToPlay = indexToFork(player2Array, totalArray: totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
            
            // Block oponent's fork
            if let nextIndexToPlay = indexToFork(player1Array, totalArray: totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
            
            // Play on Empty corner
            if let nextIndexToPlay = playOnEmptyCorner(totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
            
            // Play on Empty side
            if let nextIndexToPlay = playOnEmptySide(totalArray)
            {
                return NSIndexPath(forItem: nextIndexToPlay, inSection: 0)
            }
        }

        while (true)
        {
            // Play randomly
            let rand9 = Int(rand()%9)
            if !totalArray.contains(NSNumber(integer: rand9))
            {
                return NSIndexPath(forItem: rand9, inSection: 0)
            }
        }
    }
    
    class func indexToWin(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        if let indexForRows = checkWinningRows(array, totalArray: totalArray)
        {
            return indexForRows
        }
        
        if let indexForColumns = checkWinningColumns(array, totalArray: totalArray)
        {
            return indexForColumns
        }
        
        // First diagonal
        if (array.contains(zero) && array.contains(four))
        {
            if (!totalArray.contains(eight))
            {
                return 8
            }
        }
        else if (array.contains(zero) && array.contains(eight))
        {
            if (!totalArray.contains(four))
            {
                return 4
            }
        }
        else if (array.contains(four) && array.contains(eight))
        {
            if (!totalArray.contains(zero))
            {
                return 0
            }
        }
        
        // Second diagonal
        if (array.contains(two) && array.contains(four))
        {
            if (!totalArray.contains(six))
            {
                return 6
            }
        }
        else if (array.contains(two) && array.contains(six))
        {
            if (!totalArray.contains(four))
            {
                return 4
            }
        }
        else if (array.contains(four) && array.contains(six))
        {
            if (!totalArray.contains(two))
            {
                return 2
            }
        }
        
        return nil
    }
    
    class func indexToFork(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        // First diagonal
        if (!totalArray.contains(zero) && !totalArray.contains(four))
        {
            if (array.contains(eight))
            {
                return 0
            }
        }
        else if (!totalArray.contains(zero) && !totalArray.contains(eight))
        {
            if (array.contains(four))
            {
                return 0
            }
        }
        else if (!totalArray.contains(four) && !totalArray.contains(eight))
        {
            if (array.contains(zero))
            {
                return 4
            }
        }
        
        // Second diagonal
        if (!totalArray.contains(two) && !totalArray.contains(four))
        {
            if (array.contains(six))
            {
                return 2
            }
        }
        else if (!totalArray.contains(two) && !totalArray.contains(six))
        {
            if (array.contains(four))
            {
                return 2
            }
        }
        else if (!totalArray.contains(four) && !totalArray.contains(six))
        {
            if (array.contains(two))
            {
                return 4
            }
        }
        
        if let indexForRows = checkForkingRows(array, totalArray: totalArray)
        {
            return indexForRows
        }
        
        if let indexForColumns = checkForkingColumns(array, totalArray: totalArray)
        {
            return indexForColumns
        }
        
        
        
        return nil
    }
    
    class func checkWinningRows(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        var i = 0
        while (i < 9)
        {
            if array.contains(NSNumber(integer: i)) && array.contains(NSNumber(integer: i + 1))
            {
                if (!totalArray.contains(NSNumber(integer: i + 2)))
                {
                    return i + 2
                }
            }
            else if array.contains(NSNumber(integer: i)) && array.contains(NSNumber(integer: i + 2))
            {
                if (!totalArray.contains(NSNumber(integer: i + 1)))
                {
                    return i + 1
                }
            }
            else if array.contains(NSNumber(integer: i + 1)) && array.contains(NSNumber(integer: i + 2))
            {
                if (!totalArray.contains(NSNumber(integer: i)))
                {
                    return i
                }
            }
            i = i + 3
        }
        
        return nil
    }
 
    class func checkWinningColumns(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        var i = 0
        while (i < 3)
        {
            if array.contains(NSNumber(integer: i)) && array.contains(NSNumber(integer: i + 3))
            {
                if (!totalArray.contains(NSNumber(integer: i + 6)))
                {
                    return i + 6
                }
            }
            else if array.contains(NSNumber(integer: i)) && array.contains(NSNumber(integer: i + 6))
            {
                if (!totalArray.contains(NSNumber(integer: i + 3)))
                {
                    return i + 3
                }
            }
            else if array.contains(NSNumber(integer: i + 3)) && array.contains(NSNumber(integer: i + 6))
            {
                if (!totalArray.contains(NSNumber(integer: i)))
                {
                    return i
                }
            }
            i = i + 1
        }
        
        return nil
    }
    
    class func checkForkingRows(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        var i = 0
        while (i < 9)
        {
            if !totalArray.contains(NSNumber(integer: i)) && !totalArray.contains(NSNumber(integer: i + 1))
            {
                if (array.contains(NSNumber(integer: i + 2)))
                {
                    return i
                }
            }
            else if !totalArray.contains(NSNumber(integer: i)) && !totalArray.contains(NSNumber(integer: i + 2))
            {
                if (array.contains(NSNumber(integer: i + 1)))
                {
                    return i
                }
            }
            else if !totalArray.contains(NSNumber(integer: i + 1)) && !totalArray.contains(NSNumber(integer: i + 2))
            {
                if (array.contains(NSNumber(integer: i)))
                {
                    return i + 2
                }
            }
            i = i + 3
        }
        
        return nil
    }
    
    class func checkForkingColumns(array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        var i = 0
        while (i < 3)
        {
            if !totalArray.contains(NSNumber(integer: i)) && !totalArray.contains(NSNumber(integer: i + 3))
            {
                if (array.contains(NSNumber(integer: i + 6)))
                {
                    return i
                }
            }
            else if !totalArray.contains(NSNumber(integer: i)) && !totalArray.contains(NSNumber(integer: i + 6))
            {
                if (array.contains(NSNumber(integer: i + 3)))
                {
                    return i
                }
            }
            else if !totalArray.contains(NSNumber(integer: i + 3)) && !totalArray.contains(NSNumber(integer: i + 6))
            {
                if (array.contains(NSNumber(integer: i)))
                {
                    return i + 6
                }
            }
            i = i + 1
        }
        
        return nil
    }
    
    class func playOnCenter(totalArray: Array<NSNumber>) -> Int?
    {
        if !totalArray.contains(four)
        {
            return 4
        }
        
        return nil
    }
    
    class func playOnEmptyCorner(totalArray : Array<NSNumber>) -> Int?
    {
        let corners = [zero, two, six, eight]
        for corner in corners
        {
            if !totalArray.contains(corner)
            {
                return Int(corner)
            }
        }
        
        return nil
    }
    
    class func playOnEmptySide(totalArray: Array<NSNumber>) -> Int?
    {
        let sides = [one, three, five, seven]
        for side in sides
        {
            if !totalArray.contains(side)
            {
                return Int(side)
            }
        }
        
        return nil
    }
    
    class func indexToAvoidKillMove(player1Array: Array<NSNumber>, player2Array: Array<NSNumber>, totalArray: Array<NSNumber>) -> Int?
    {
        if (((player1Array.contains(zero) && player1Array.contains(eight)) || (player1Array.contains(two) && player1Array.contains(six))) && player2Array.contains(four) && player2Array.count == 1)
        {
            return 1
        }
        
        if ((player1Array.contains(two) || (player1Array.contains(five))) && (player1Array.contains(six) || player1Array.contains(seven)) && totalArray.count == 3)
        {
            return 8
        }
        
        return nil
    }
    
    class func randomMove(totalArray: Array<NSNumber>) -> Int?
    {
        while (true)
        {
            let randomIndex = Int(rand()%9)
            if !totalArray.contains(NSNumber(integer: randomIndex))
            {
                return randomIndex
            }
        }
    }
}
