//
//  MTTMenuViewController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var currentMode = ""
    var currentDifficulty = ""
    let tableView = UITableView()
    let backgroundImageView = MTTAnimatedImageView(image: nil)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        currentMode = MTTUtils.getCurrentMode()
        currentDifficulty = MTTUtils.getCurrentDifficulty()
        
        self.setUpUI()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateTheme",
            name: CHANGE_THEME,
            object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpUI()
    {
        self.navigationItem.title = NSLocalizedString("menu", comment: "")
        self.view.addSubview(self.tableView)
        self.backgroundImageView.update()
        
        self.tableView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(self.view)
        }
        
        self.tableView.backgroundColor = MTTStyle.getThirdColor()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerClass(MTTTableViewCell.self, forCellReuseIdentifier: TABLE_VIEW_CELL_ID)
        // Hide extra separators
        self.tableView.tableFooterView = UIView()

        self.tableView.backgroundView = backgroundImageView
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch(section)
        {
        case 0:
            return 2
        case 1:
            return 2
        case 2:
            return 1
        case 3:
            return 3
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(TABLE_VIEW_CELL_ID, forIndexPath: indexPath) as! MTTTableViewCell
        cell.update()
        
        switch (indexPath.section)
        {
        case 0:
            switch (indexPath.row)
            {
            case 0:
                cell.textLabel?.text = NSLocalizedString("normal", comment: "")
                cell.accessoryType = ( currentDifficulty == DIFFICULTY_NORMAL ) ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            case 1:
                cell.textLabel?.text = NSLocalizedString("hard", comment: "")
                cell.accessoryType = ( currentDifficulty == DIFFICULTY_HARD ) ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
            default:
                cell.textLabel?.text = ""
            }
        case 1:
            switch (indexPath.row)
            {
            case 0:
                
                cell.textLabel?.text = NSLocalizedString("player_vs_computer", comment: "")
                cell.accessoryType = ( currentMode == PLAYER_VS_COMPUTER_MODE ) ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
                
            case 1:
                
                cell.textLabel?.text = NSLocalizedString("player_vs_player", comment: "")
                cell.accessoryType = ( currentMode == PLAYER_VS_PLAYER_MODE ) ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
    
            default:
                break
            }
        case 2:
            switch (indexPath.row)
            {
            case 0:
                cell.textLabel?.text = NSLocalizedString("change_theme", comment: "")
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            default:
                break
            }
        case 3:
            switch (indexPath.row)
            {
            case 0:
                
                cell.textLabel?.text = NSLocalizedString("sound", comment: "")
                
                let switchButton = UISwitch()
                switchButton.addTarget(self, action: "changeSoundStatus", forControlEvents: UIControlEvents.ValueChanged)
                let switchIsOn = MTTUtils.soundIsEnabled() ? true : false
                switchButton.setOn(switchIsOn, animated: false)
                cell.accessoryView = switchButton

                cell.selectionStyle = UITableViewCellSelectionStyle.None
                
            case 1:
                cell.textLabel?.text = NSLocalizedString("woman_voice", comment: "")
                cell.accessoryType = ( MTTUtils.getCurrentSoundType() == SOUND_TYPE_WOMAN && MTTUtils.soundIsEnabled() ) ? .Checkmark : .None
                cell.accessoryView?.alpha = MTTUtils.soundIsEnabled() ? 1.0 : 0.5
                cell.userInteractionEnabled = MTTUtils.soundIsEnabled()
                cell.textLabel?.enabled = MTTUtils.soundIsEnabled()
            case 2:
                cell.textLabel?.text = NSLocalizedString("man_voice", comment: "")
                cell.accessoryType = ( MTTUtils.getCurrentSoundType() == SOUND_TYPE_MAN && MTTUtils.soundIsEnabled() ) ? .Checkmark : .None
                cell.accessoryView?.alpha = MTTUtils.soundIsEnabled() ? 1.0 : 0.5
                cell.userInteractionEnabled = MTTUtils.soundIsEnabled()
                cell.textLabel?.enabled = MTTUtils.soundIsEnabled()
            default:
                break
            }
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = MTTHeaderView(frame: CGRectMake(0, 0, 0, 0))
        
        switch (section) {
        case 0:
            headerCell.label.text = NSLocalizedString("difficulty", comment: "")
        case 1:
            headerCell.label.text = NSLocalizedString("mode", comment: "")
        case 2:
            headerCell.label.text = NSLocalizedString("themes", comment: "")
        case 3:
            headerCell.label.text = NSLocalizedString("other_settings", comment: "")
        default:
            break
        }
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return HEIGHT_TABLE_VIEW_HEADER
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath)
        {
            cell.selected = false
            switch (indexPath.section)
            {
            case 0:
                switch (indexPath.row)
                {
                case 0:
                    changeCurrentDifficultyTo(DIFFICULTY_NORMAL)
                case 1:
                    changeCurrentDifficultyTo(DIFFICULTY_HARD)
                default:
                    break
                }
            case 1:
                switch (indexPath.row)
                {
                case 0:
                    changeCurrentModeTo(PLAYER_VS_COMPUTER_MODE)
                case 1:
                    changeCurrentModeTo(PLAYER_VS_PLAYER_MODE)
                default:
                    break
                }
            case 2:
                switch (indexPath.row)
                {
                case 0:
                    let statisticsVC = MTTThemesMenuTableViewController()
                    self.navigationController?.pushViewController(statisticsVC, animated: true)
                default:
                    break
                }
            case 3:
                switch (indexPath.row)
                {
                case 1:
                    MTTUtils.setSoundType(SOUND_TYPE_WOMAN)
                case 2:
                    MTTUtils.setSoundType(SOUND_TYPE_MAN)
                default:
                    break
                }
            default:
                break
            }
            
            self.tableView.reloadData()
        }
    }
    
    func changeCurrentModeTo(mode: String)
    {
        MTTUtils.setCurrentModeTo(mode)
        currentMode = mode
        GAME_VIEW_CONTROLLER.currentMode = mode
        GAME_VIEW_CONTROLLER.reloadGame()
    }

    func changeCurrentDifficultyTo(difficulty: String)
    {
        MTTUtils.setCurrentDifficultyTo(difficulty)
        currentDifficulty = difficulty
    }
    
    func changeSoundStatus()
    {
        MTTUtils.setSoundEnabled(!MTTUtils.soundIsEnabled())
        self.tableView.reloadData()
    }
    
    func updateTheme()
    {
        self.tableView.backgroundColor = MTTStyle.getThirdColor()
        self.backgroundImageView.update()
        self.tableView.reloadData()
    }
}
