//
//  MTTStatisticsViewController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import RealmSwift
import Charts

class MTTStatisticsViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, ChartViewDelegate {
    
    var scrollView = UIScrollView()
    var backgroundImageView = MTTAnimatedImageView(image: nil)
    var pageControl = UIPageControl()
    var lastContentOffset = CGFloat()
    var isScrolling = false
    var firstView = UIView()
    var secondView = UIView()
    var thirdView = UIView()
    var fourthView = UIView()
    var fifthView = UIView()
    var offset = CGFloat()
    var barMenuImageView = UIImageView()
    var editContactButtonImageView = UIImageView()
    var tableView = UITableView()
    var tableViewFooter : MTTFooterView?
    let realm = try! Realm()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateAllViews",
            name: RELOAD_STATISTICS,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateTheme",
            name: CHANGE_THEME,
            object: nil)
        self.setUpUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpUI()
    {
        self.navigationItem.title = NSLocalizedString("statistics", comment: "")
        let numberOfPages = 3
        
        self.view.addSubview(self.backgroundImageView)
        
        self.backgroundImageView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(self.view)
        }
        
        // SCROLL VIEW
        let scrollViewFrame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height)
        scrollView = UIScrollView(frame: scrollViewFrame)
        scrollView.delegate = self
        self.view.addSubview(scrollView)
        let scrollViewContentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width*CGFloat(numberOfPages), scrollView.frame.size.height)
        self.backgroundImageView.addSubview(self.scrollView)
        self.backgroundImageView.update()
        scrollView.contentSize = scrollViewContentSize
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.scrollEnabled = true
        scrollView.backgroundColor = MTTThemesManager.getCurrentTheme() == HELL_THEME ? UIColor.clearColor() : MTTStyle.getThirdColor()
        self.pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        self.pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
        self.pageControl.userInteractionEnabled = false
        
        // PAGE CONTROL
        pageControl.frame = CGRectMake(self.view.center.x - UIScreen.mainScreen().bounds.size.width/8, 1.55*self.view.center.y, UIScreen.mainScreen().bounds.size.width/4, UIScreen.mainScreen().bounds.size.height/5)
        pageControl.numberOfPages = numberOfPages
        pageControl.currentPage = 0
        self.view.addSubview(pageControl)
        pageControl.backgroundColor = UIColor.clearColor()
        
        offset = scrollView.contentSize.width / CGFloat(pageControl.numberOfPages)
        
        self.loadAllViews()
    }
    
    func setBackgroundImageView()
    {
        self.backgroundImageView.userInteractionEnabled = true
        
        if MTTThemesManager.getCurrentTheme() == HELL_THEME
        {
            self.backgroundImageView.animationImages = MTTUtils.getAnimatedImagesForTheme(HELL_THEME)
            self.backgroundImageView.startAnimating()
        }
        else
        {
            self.backgroundImageView.animationImages = nil
        }
    }
    
    func loadAllViews()
    {
        // FIRST PAGE
        firstView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        firstView.backgroundColor = UIColor.clearColor()
        self.scrollView.addSubview(firstView)
        
        self.populateFirstView()
        
        // SECOND PAGE
        secondView = UIView(frame: CGRectMake(offset, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        secondView.backgroundColor = UIColor.clearColor()
        self.scrollView.addSubview(secondView)
        
        self.populateSecondView()
        
        // THIRD PAGE
        thirdView = UIView(frame: CGRectMake(2*offset, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        thirdView.backgroundColor = UIColor.clearColor()
        self.scrollView.addSubview(thirdView)
        
        self.populateThirdView()
        
        // FOURTH PAGE
        fourthView = UIView(frame: CGRectMake(3*offset, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        fourthView.backgroundColor = UIColor.clearColor()
        self.scrollView.addSubview(fourthView)
        
        self.populateFourthView()
        
        // FIFTH PAGE
        fifthView = UIView(frame: CGRectMake(4*offset, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        fifthView.backgroundColor = UIColor.clearColor()
        self.scrollView.addSubview(fifthView)
        
        self.populateFifthView()
    }
    
    func updateAllViews()
    {
        MTTUtils.removeAllSubviewsFrom(firstView)
        MTTUtils.removeAllSubviewsFrom(secondView)
        MTTUtils.removeAllSubviewsFrom(thirdView)
        MTTUtils.removeAllSubviewsFrom(fourthView)
        MTTUtils.removeAllSubviewsFrom(fifthView)
        
        self.populateFirstView()
        self.populateSecondView()
        self.populateThirdView()
        self.populateFourthView()
        self.populateFifthView()
        
        self.tableView.reloadData()
    }
    
    func populateFirstView() {
        
        self.tableView.registerClass(MTTTableViewCell.self, forCellReuseIdentifier: TABLE_VIEW_CELL_ID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = MTTThemesManager.getCurrentTheme() == HELL_THEME ? UIColor.clearColor() : MTTStyle.getThirdColor()
        
        self.tableView.scrollEnabled = true
        self.tableView.userInteractionEnabled = true
        
        // Hide extra separators
        self.tableViewFooter = MTTFooterView(frame: CGRectMake(0, 0, 0, 80))
        self.tableViewFooter!.label.text = NSLocalizedString("statistics_only_against_computer", comment: "")
        self.tableView.tableFooterView = self.tableViewFooter
        
        self.firstView.addSubview(self.tableView)
        
        let tableViewInsets = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
        self.tableView.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.firstView)
            make.left.equalTo(self.firstView)
            make.top.equalTo(self.firstView).inset(tableViewInsets)
            make.bottom.equalTo(self.firstView)
        }
        
        self.tableView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, 1000)
    }
    
    func populateSecondView() {
        
        let gamesWon = realm.objects(RLMGame).filter("result = 'player' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
        let gamesLost = realm.objects(RLMGame).filter("result = 'computer' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
        let drawGames = realm.objects(RLMGame).filter("result = 'draw' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
        let games = realm.objects(RLMGame).filter("mode = %@", PLAYER_VS_COMPUTER_MODE)
        
        var winRate = (Double(gamesWon.count) / Double(games.count)) * 100
        var loseRate = (Double(gamesLost.count) / Double(games.count)) * 100
        var drawRate = (Double(drawGames.count) / Double(games.count)) * 100
        winRate = winRate.isNaN ? 0 : winRate
        loseRate = loseRate.isNaN ? 0 : loseRate
        drawRate = drawRate.isNaN ? 0 : drawRate
        
        let pieChartView = PieChartView()
        pieChartView.delegate = self
        
        let dataSets =  [winRate, loseRate, drawRate]
        let sliceLabels = [NSLocalizedString("win", comment: ""), NSLocalizedString("lost", comment: ""), NSLocalizedString("draw_label", comment: "")]
        
        setPieChart(pieChartView, dataPoints: sliceLabels, values: dataSets)
        
        let insets = UIEdgeInsets(top: 64, left: 0, bottom: 90, right: 0)
        self.secondView.addSubview(pieChartView)
        pieChartView.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.secondView)
            make.left.equalTo(self.secondView)
            make.top.equalTo(self.secondView).inset(insets)
            make.bottom.equalTo(self.secondView).inset(insets)
        }
    }
    
    // Third page : bar chart representing number times a move has been first and winning percentage of player ( or computer ) making this move first ( corner, center, side )
    func populateThirdView()
    {
        let gamesWhereFirstMoveIsCorner = realm.objects(RLMGame).filter("(firstMove = %@ OR firstMove = %@ OR firstMove = %@ OR firstMove = %@) AND mode = %@", zero, two, six, eight, PLAYER_VS_COMPUTER_MODE)
        let gamesWhereFirstMoveIsCenter = realm.objects(RLMGame).filter("firstMove = %@ AND mode = %@", four, PLAYER_VS_COMPUTER_MODE)
        let gamesWhereFirstMoveIsSide = realm.objects(RLMGame).filter("(firstMove = %@ OR firstMove = %@ OR firstMove = %@ OR firstMove = %@) AND mode = %@", one, three, five, seven, PLAYER_VS_COMPUTER_MODE)
        
        let gamesWonWhenFirstMoveIsCorner = realm.objects(RLMGame).filter("(firstMove = %@ OR firstMove = %@ OR firstMove = %@ OR firstMove = %@) AND result = firstPlayer AND mode = %@", zero, two, six, eight, PLAYER_VS_COMPUTER_MODE)
        let gamesWonWhenFirstMoveIsCenter = realm.objects(RLMGame).filter("firstMove = %@ AND result = firstPlayer AND mode = %@", four, PLAYER_VS_COMPUTER_MODE)
        let gamesWonWhenFirstMoveIsSide = realm.objects(RLMGame).filter("(firstMove = %@ OR firstMove = %@ OR firstMove = %@ OR firstMove = %@) AND result = firstPlayer AND mode = %@", one, three, five, seven, PLAYER_VS_COMPUTER_MODE)
        
        var cornerWinRate = Double(( Double(gamesWonWhenFirstMoveIsCorner.count) / Double(gamesWhereFirstMoveIsCorner.count) ) * 100)
        var centerWinRate = Double(( Double(gamesWonWhenFirstMoveIsCenter.count) / Double(gamesWhereFirstMoveIsCenter.count) ) * 100)
        var sideWinRate = Double(( Double(gamesWonWhenFirstMoveIsSide.count) / Double(gamesWhereFirstMoveIsSide.count) ) * 100)
        cornerWinRate = cornerWinRate.isNaN ? 0 : cornerWinRate
        centerWinRate = centerWinRate.isNaN ? 0 : centerWinRate
        sideWinRate = sideWinRate.isNaN ? 0 : sideWinRate
        
        let barChartView = BarChartView()
        barChartView.delegate = self
        
        let dataSets =  [[Double(gamesWhereFirstMoveIsCorner.count), cornerWinRate], [Double(gamesWhereFirstMoveIsCenter.count), centerWinRate], [Double(gamesWhereFirstMoveIsSide.count), sideWinRate]]
        let xAxeLabels = [NSLocalizedString("corner", comment: ""), NSLocalizedString("center", comment: ""), NSLocalizedString("side", comment: "")]

        setBarChart(barChartView, dataPoints: xAxeLabels, values: dataSets)
        
        let insets = UIEdgeInsets(top: 64, left: 0, bottom: 90, right: 0)
        self.thirdView.addSubview(barChartView)
        barChartView.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.thirdView)
            make.left.equalTo(self.thirdView)
            make.top.equalTo(self.thirdView).inset(insets)
            make.bottom.equalTo(self.thirdView).inset(insets)
        }
    }
    
    func populateFourthView()
    {
        
    }
    
    func populateFifthView()
    {
        
    }
    
    func setPieChart(pieChartView: PieChartView, dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        pieChartView.noDataText = NSLocalizedString("no_data", comment: "")
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "( % )")
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.descriptionText = ""
        
        var colors: [UIColor] = []
        
        for _ in 0..<dataPoints.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        
        pieChartDataSet.colors = colors
    }
    
    func setBarChart(barChartView : BarChartView, dataPoints: [String], values: [[Double]])
    {
        barChartView.noDataText = NSLocalizedString("no_data", comment: "")
        barChartView.userInteractionEnabled = false
        var firstDataEntries: [BarChartDataEntry] = []
        var secondDataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let firstDataEntry = BarChartDataEntry(value: values[i][0], xIndex: i)
            let secondDataEntry = BarChartDataEntry(value: values[i][1], xIndex: i)
            firstDataEntries.append(firstDataEntry)
            secondDataEntries.append(secondDataEntry)
        }
        
        let firstChartDataSet = BarChartDataSet(yVals: firstDataEntries, label: NSLocalizedString("played_as_first_move", comment: ""))
        let secondChartDataset = BarChartDataSet(yVals: secondDataEntries, label: NSLocalizedString("victories_percentage", comment: ""))
        let chartData = BarChartData(xVals: dataPoints, dataSets: [firstChartDataSet, secondChartDataset])
        barChartView.data = chartData
        
        barChartView.descriptionText = ""
        
        firstChartDataSet.colors = [UIColor(red: 250/255, green: 146/255, blue: 54/255, alpha: 1)]
        secondChartDataset.colors = [UIColor(red: 200/255, green: 96/255, blue: 4/255, alpha: 1)]
        
        barChartView.xAxis.labelPosition = .Bottom
        
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch(section)
        {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(TABLE_VIEW_CELL_ID, forIndexPath: indexPath) as! MTTTableViewCell
        cell.userInteractionEnabled = false
        cell.update()
        
        switch (indexPath.section)
        {
        case 0:
            switch (indexPath.row)
            {
            case 0:
                let games = realm.objects(RLMGame).filter("mode = %@", PLAYER_VS_COMPUTER_MODE)
                cell.textLabel?.text = NSLocalizedString("games_played", comment: "") + String(games.count)
            default:
                break
            }
        case 1:
            switch (indexPath.row)
            {
            case 0:
                let gamesWonInNormalMode = realm.objects(RLMGame).filter("difficulty = 'Normal' AND result = 'player' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                let gamesLostInNormalMode = realm.objects(RLMGame).filter("difficulty = 'Normal' AND result = 'computer' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                let drawGamesInNormalMode = realm.objects(RLMGame).filter("difficulty = 'Normal' AND result = 'draw' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                cell.textLabel?.text = NSLocalizedString("victories", comment: "") + " : " + String(gamesWonInNormalMode.count) +  "  |  " + NSLocalizedString("defeats", comment: "") + " : " + String(gamesLostInNormalMode.count) +  "  |  " + NSLocalizedString("draws", comment: "") + " : " + String(drawGamesInNormalMode.count)
                
            default:
                break
            }
        case 2:
            switch (indexPath.row)
            {
            case 0:
                let gamesWonInHardMode = realm.objects(RLMGame).filter("difficulty = 'Hard' AND result = 'player' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                let gamesLostInHardMode = realm.objects(RLMGame).filter("difficulty = 'Hard' AND result = 'computer' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                let drawGamesInHardMode = realm.objects(RLMGame).filter("difficulty = 'Hard' AND result = 'draw' AND mode = %@", PLAYER_VS_COMPUTER_MODE)
                cell.textLabel?.text = NSLocalizedString("victories", comment: "") + " : " + String(gamesWonInHardMode.count) +  "  |  " + NSLocalizedString("defeats", comment: "") + " : " + String(gamesLostInHardMode.count) +  "  |  " + NSLocalizedString("draws", comment: "") + " : " + String(drawGamesInHardMode.count)
            default:
                break
            }
            
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = MTTHeaderView(frame: CGRectMake(0, 0, 0, 0))
        
        switch (section) {
        case 0:
            headerCell.label.text = NSLocalizedString("general_info", comment: "")
        case 1:
            headerCell.label.text = NSLocalizedString("difficulty", comment: "") + ": " + NSLocalizedString("normal", comment: "")
        case 2:
            headerCell.label.text = NSLocalizedString("difficulty", comment: "") + ": " +  NSLocalizedString("hard", comment: "")
        default:
            break
        }
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 25
    }
    
    // Mark - Scroll View Delegate Methods
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        // Update page control
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        self.pageControl.currentPage = Int(page)
        
        switch self.pageControl.currentPage
        {
        case 0:
            self.navigationItem.title = NSLocalizedString("statistics", comment: "")
        case 1:
            self.navigationItem.title = NSLocalizedString("general", comment: "")
        case 2:
            self.navigationItem.title = NSLocalizedString("first_move", comment: "")
        default:
            break
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        // Disable vertical scrolling
        if scrollView.contentOffset.y != 0
        {
            var tableViewOffset = self.tableView.contentOffset
            tableViewOffset.y = scrollView.contentOffset.y
            self.tableView.contentOffset = tableViewOffset
            var offset = scrollView.contentOffset
            offset.y = 0
            scrollView.contentOffset = offset
            
        }
    }
    
    func updateTheme()
    {
        self.scrollView.backgroundColor = MTTStyle.getThirdColor()
        self.tableView.backgroundColor = MTTStyle.getThirdColor()
        self.tableViewFooter?.label.textColor = MTTStyle.getSecondColor()
        self.scrollView.backgroundColor = MTTThemesManager.getCurrentTheme() == HELL_THEME ? UIColor.clearColor() : MTTStyle.getThirdColor()
        self.tableView.backgroundColor = MTTThemesManager.getCurrentTheme() == HELL_THEME ? UIColor.clearColor() : MTTStyle.getThirdColor()
        self.backgroundImageView.update()
        self.tableView.reloadData()
    }
}
