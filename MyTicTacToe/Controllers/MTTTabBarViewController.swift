//
//  MTTTabBarViewController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 24/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateTheme",
            name: CHANGE_THEME,
            object: nil)
        
        self.setUpColors()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpColors()
    {
        self.tabBar.tintColor = MTTStyle.getMainColor()
        self.tabBar.alpha = 0.9
    }
    
    func updateTheme()
    {
        self.setUpColors()
    }
}
