//
//  MTTThemesMenuTableViewController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 09/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTThemesMenuTableViewController: UITableViewController {

    let backgroundImageView = MTTAnimatedImageView(image: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("themes", comment: "")
        self.backgroundImageView.update()
        self.setUpTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setUpTableView()
    {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerClass(MTTTableViewCell.self, forCellReuseIdentifier: TABLE_VIEW_CELL_ID)
        self.tableView.backgroundColor = MTTStyle.getThirdColor()
        // Hide extra separators
        self.tableView.tableFooterView = UIView()
        
        self.tableView.backgroundView = backgroundImageView
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier(TABLE_VIEW_CELL_ID, forIndexPath: indexPath) as! MTTTableViewCell
        cell.update()

        switch indexPath.row
        {
        case 0:
            cell.textLabel?.text = NSLocalizedString("green_theme", comment: "")
            cell.accessoryType = MTTThemesManager.getCurrentTheme() == GREEN_THEME ? .Checkmark : .None
        case 1:
            cell.textLabel?.text = NSLocalizedString("blue_theme", comment: "")
            cell.accessoryType = MTTThemesManager.getCurrentTheme() == BLUE_THEME ? .Checkmark : .None
        case 2:
            cell.textLabel?.text = NSLocalizedString("hell_theme", comment: "")
            cell.accessoryType = MTTThemesManager.getCurrentTheme() == HELL_THEME ? .Checkmark : .None
        default:
            break;
        }
        
        return cell
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = MTTHeaderView(frame: CGRectMake(0, 0, 0, 0))
        
        switch (section) {
        case 0:
            headerCell.label.text = NSLocalizedString("themes", comment: "")
        default:
            break
        }
        
        return headerCell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return HEIGHT_TABLE_VIEW_HEADER
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if let cell = tableView.cellForRowAtIndexPath(indexPath)
        {
            cell.selected = false
            
            switch indexPath.row
            {
            case 0:
                changeToTheme(GREEN_THEME)
            case 1:
                changeToTheme(BLUE_THEME)
            case 2:
                changeToTheme(HELL_THEME)
            default:
                break;
            }
            
            cell.tintColor = MTTStyle.getMainColor()
        }
    }
    
    func changeToTheme(theme: String)
    {
        if MTTThemesManager.getCurrentTheme() == theme
        {
            return
        }
        
        MTTThemesManager.changeToTheme(theme)
        self.backgroundImageView.update()
        
        self.tableView.reloadData()
        UIView.animateWithDuration(ANIMATION_THEME_CHANGE, animations: {
            self.tableView.backgroundColor = MTTStyle.getThirdColor()
            }, completion: {
                (value: Bool) in
        })
    }
}
