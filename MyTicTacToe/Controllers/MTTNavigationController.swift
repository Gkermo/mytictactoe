//
//  MTTNavigationController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateTheme",
            name: CHANGE_THEME,
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpColors()
    {
        self.navigationBar.barTintColor = MTTStyle.getMainColor()
        self.navigationBar.tintColor = MTTStyle.getFourthColor()
        self.navigationController?.navigationBar.tintColor = MTTStyle.getFourthColor()
        self.navigationBar.titleTextAttributes = NSDictionary(object: MTTStyle.getFourthColor(), forKey: NSForegroundColorAttributeName) as? [String : AnyObject]
    }

    func updateTheme()
    {
        UIView.animateWithDuration(ANIMATION_THEME_CHANGE, animations: {
            self.setUpColors()
        })
    }
}
