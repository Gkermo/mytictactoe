//
//  MTTGameViewController.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 24/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import SnapKit

class MTTGameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var boardContainerView = UIView()
    var navigationTitleView = MTTLabelWithImage()
    var backgroundImageView = MTTAnimatedImageView(image: nil)
    var collectionView: UICollectionView?
    var player1Turn = true
    var player1Starts = true
    var player1Array = Array<NSNumber>()
    var player2Array = Array<NSNumber>()
    var firstMove = NSNumber(integer: 99)
    var winner = ""
    var animationDuration = 0.5
    var currentMode = ""
    var currentModeLabel : MTTLabel?
    var scorePlayer1Label : MTTLabelWithImage?
    var scorePlayer2Label : MTTLabelWithImage?
    var player1Score = 0
    var player2Score = 0
    var currentPlayer2 = ""
    var currentPlayer1 = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        currentMode = MTTUtils.getCurrentMode()
        self.setCurrentPlayers()
        self.setUpUI()
        self.updateSounds()
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateTheme",
            name: CHANGE_THEME,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateSounds",
            name: CHANGE_SOUNDS,
            object: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func setCurrentPlayers()
    {
        if currentMode == PLAYER_VS_COMPUTER_MODE
        {
            currentPlayer1 = NSLocalizedString("player", comment: "")
            currentPlayer2 = NSLocalizedString("computer", comment: "")
        }
        else
        {
            currentPlayer1 = NSLocalizedString("player_1", comment: "")
            currentPlayer2 = NSLocalizedString("player_2", comment: "")
        }
    }
    
    func setUpUI()
    {
        self.navigationItem.title = self.currentPlayer1
        
        self.view.addSubview(backgroundImageView)
        
        self.backgroundImageView.update()
        
        self.backgroundImageView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(self.view)
        }
        
        currentModeLabel = MTTLabel()
        scorePlayer1Label = MTTLabelWithImage(text: currentPlayer1 + " : " + String(player1Score), image: UIImage(named: "X_small")!)
        scorePlayer2Label = MTTLabelWithImage(text: currentPlayer2 + " : " + String(player2Score), image: UIImage(named: "O_small")!)
        
        self.backgroundImageView.addSubview(self.boardContainerView)
        self.backgroundImageView.addSubview(self.currentModeLabel!)
        self.backgroundImageView.addSubview(self.scorePlayer1Label!)
        self.backgroundImageView.addSubview(self.scorePlayer2Label!)
        
        self.backgroundImageView.backgroundColor = MTTStyle.getThirdColor()
        
        let viewWidth = self.view.bounds.size.width
        
        // Current mode label
        self.currentModeLabel!.text = NSLocalizedString("mode", comment: "") + " : " + NSLocalizedString(MTTUtils.getCurrentMode(), comment: "")

        let currentModeLabelInsets = UIEdgeInsets(top: 24, left: 10, bottom: 0, right: 0)
        self.currentModeLabel!.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.backgroundImageView)
            make.left.equalTo(self.backgroundImageView).inset(currentModeLabelInsets)
            make.top.equalTo(self.backgroundImageView).inset(currentModeLabelInsets)
            make.bottom.equalTo(self.boardContainerView.snp_top)
        }
        
        // Board
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: -64, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: viewWidth/3 - 4, height: viewWidth/3 - 4)
        layout.minimumInteritemSpacing = MARGIN
        layout.minimumLineSpacing = MARGIN
        self.collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        self.collectionView?.backgroundColor = MTTStyle.getFourthColor()
        self.collectionView?.alpha = ELEMENTS_ALPHA
        self.collectionView?.scrollEnabled = false
        self.boardContainerView.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.backgroundImageView)
            make.left.equalTo(self.backgroundImageView)
            make.center.equalTo(self.backgroundImageView)
            make.height.equalTo(viewWidth)
        }
        
        self.boardContainerView.addSubview(self.collectionView!)
        
        let boardInsets = UIEdgeInsetsMake(MARGIN, MARGIN, MARGIN, MARGIN)
        self.collectionView!.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.boardContainerView).inset(boardInsets)
            make.left.equalTo(self.boardContainerView).inset(boardInsets)
            make.top.equalTo(self.boardContainerView).inset(boardInsets)
            make.bottom.equalTo(self.boardContainerView).inset(boardInsets)
        }
        
        // Scores labels
        self.reloadScore()
        
        let scoresInsets = UIEdgeInsetsMake(10, 10, 60, 10)
        self.scorePlayer1Label!.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(self.backgroundImageView).inset(scoresInsets)
            make.top.equalTo(self.boardContainerView.snp_bottom)
            make.bottom.equalTo(self.backgroundImageView).inset((self.tabBarController?.tabBar.bounds.height)!)
        }
        self.scorePlayer2Label!.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(self.backgroundImageView).inset(scoresInsets)
            make.bottom.equalTo(self.backgroundImageView).inset((self.tabBarController?.tabBar.bounds.height)!)
            make.top.equalTo(self.boardContainerView.snp_bottom)
        }
        
        self.scorePlayer1Label?.updateLayout()
        self.scorePlayer2Label?.updateLayout()
        
        self.collectionView?.registerClass(MTTCollectionViewCell.self, forCellWithReuseIdentifier: COLLECTION_VIEW_CELL_ID)
        
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
    }
    
    func setUpAudioEffects()
    {
        AudioSamplePlayer.sharedInstance().preloadAudioSample("pop")
        AudioSamplePlayer.sharedInstance().preloadAudioSample(MTTUtils.getSoundIdentifier(SOUND_LETS_PLAY))
        AudioSamplePlayer.sharedInstance().preloadAudioSample(MTTUtils.getSoundIdentifier(SOUND_DRAW))
        AudioSamplePlayer.sharedInstance().preloadAudioSample(MTTUtils.getSoundIdentifier(SOUND_LOST))
        AudioSamplePlayer.sharedInstance().preloadAudioSample(MTTUtils.getSoundIdentifier(SOUND_WIN))
        
        self.playSound(MTTUtils.getSoundIdentifier(SOUND_LETS_PLAY))
    }
    
    func loadNewGame()
    {
        self.collectionView?.userInteractionEnabled = false
        player1Array = Array<NSNumber>()
        player2Array = Array<NSNumber>()
        winner = ""
        firstMove = NSNumber(integer: 99)
        
        for var i = 0; i < 9; i++
        {
            let indexPath = NSIndexPath(forItem: i, inSection: 0)
            if let cell : MTTCollectionViewCell = collectionView!.cellForItemAtIndexPath(indexPath) as? MTTCollectionViewCell
            {
                cell.backgroundView = nil
            }
        }
        collectionView?.reloadData()
        
        if (!player1Starts && currentMode == PLAYER_VS_COMPUTER_MODE)
        {
            self.computerPlay()
        }
        else
        {
            self.collectionView?.userInteractionEnabled = true
        }
        
        reloadScore()
    }
    
    func reloadGame()
    {
        player1Starts = true
        player1Turn = true
        self.currentModeLabel!.text = NSLocalizedString("mode", comment: "") + " : " + NSLocalizedString(MTTUtils.getCurrentMode(), comment: "")
        player1Score = 0
        player2Score = 0
        self.setCurrentPlayers()
        self.navigationItem.title = self.currentPlayer1
        loadNewGame()
    }
    
    func reloadScore()
    {
        self.scorePlayer1Label!.label.text = currentPlayer1 + " : " + String(player1Score)
        self.scorePlayer2Label!.label.text = currentPlayer2 + " : " + String(player2Score)
    }
    
    // MARK: - UICollectionViewDataSource methods
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 9
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(COLLECTION_VIEW_CELL_ID, forIndexPath: indexPath)
        cell.backgroundColor = MTTStyle.getMainColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if (firstMove == NSNumber(integer: 99))
        {
            firstMove = NSNumber(integer: indexPath.row)
        }
        self.updateItem(collectionView, atIndexPath: indexPath)
    }
    
    func updateItem(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath)
    {
        if let cell : MTTCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as? MTTCollectionViewCell
        {
            if !(cell.backgroundView is UIImageView)
            {
                self.playSound("pop")
                
                let imageView = UIImageView()
                imageView.alpha = 0.0
                imageView.tintColor = MTTStyle.getFourthColor()
                
                if player1Turn
                {
                    imageView.image = UIImage(named: "X")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                    cell.player1 = true
                    player1Array.append(NSNumber(integer: indexPath.row))
                }
                else
                {
                    imageView.image = UIImage(named: "O")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                    cell.player2 = true
                    player2Array.append(NSNumber(integer: indexPath.row))
                }
                self.player1Turn = !self.player1Turn
                cell.backgroundView = imageView
                UIView.animateWithDuration(animationDuration, animations: {
                    
                    cell.backgroundView?.alpha = 1.0
                    
                    }, completion: { (value: Bool) in
                        
                })
            }
        }
        
        self.navigationItem.title = self.player1Turn ? self.currentPlayer1 : self.currentPlayer2
        
        let gameIsFinished = self.manageEndOfGame()
        if (!gameIsFinished)
        {
            if (currentMode == PLAYER_VS_COMPUTER_MODE && !player1Turn)
            {
                self.computerPlay()
            }
        }
    }
    
    // MARK - Game methods
    func computerPlay()
    {
        self.collectionView?.userInteractionEnabled = false
        
        dispatch_async(dispatch_queue_create("com.MyTicTacToe.computerPlayQueue", nil), {
            
            NSThread.sleepForTimeInterval(self.animationDuration)
            if let indexOfCellToTap = MTTAI.play(self.collectionView!, player1Array: self.player1Array, player2Array: self.player2Array)
            {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.collectionView?.selectItemAtIndexPath(indexOfCellToTap, animated: true, scrollPosition: UICollectionViewScrollPosition.None)
                    self.updateItem(self.collectionView!, atIndexPath: indexOfCellToTap)
                    self.collectionView?.userInteractionEnabled = true
                })
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), {
                    
                    self.displayEndOfGameMessageAndStartNewGame(NSLocalizedString("error", comment: ""), message: "")
                })
            }
        })
    }
    
    func manageEndOfGame() -> Bool
    {
        var gameIsFinished = false
        let firstPlayer = self.player1Starts ? self.currentPlayer1 : self.currentPlayer2
        
        if arrayContainsVictoriousCombination(player1Array)
        {
            gameIsFinished = true
            winner = currentPlayer1
            displayEndOfGameMessageAndStartNewGame(winner + NSLocalizedString("wins", comment: ""), message: "")
            player1Score++
            
            if MTTUtils.getCurrentMode() == PLAYER_VS_COMPUTER_MODE
            {
               self.playSound(MTTUtils.getSoundIdentifier(SOUND_WIN))
            }
        }
        else if arrayContainsVictoriousCombination(player2Array)
        {
            gameIsFinished = true
            winner = currentPlayer2
            displayEndOfGameMessageAndStartNewGame(winner + NSLocalizedString("wins", comment: ""), message: "")
            player2Score++
            
            if MTTUtils.getCurrentMode() == PLAYER_VS_COMPUTER_MODE
            {
                self.playSound(MTTUtils.getSoundIdentifier(SOUND_LOST))
            }
        }
        else if equality()
        {
            gameIsFinished = true
            winner = DRAW
            displayEndOfGameMessageAndStartNewGame(NSLocalizedString("draw", comment: ""), message: "")
            self.playSound(MTTUtils.getSoundIdentifier(SOUND_DRAW))
        }
        
        if gameIsFinished
        {
            self.player1Starts = !self.player1Starts
            self.player1Turn = self.player1Starts
            self.navigationItem.title = self.player1Turn ? self.currentPlayer1 : self.currentPlayer2
            let dbWinner = MTTUtils.setNormalizedDbValue(self.winner)
            let dbFirstPlayer = MTTUtils.setNormalizedDbValue(firstPlayer)
            let dbFirstMove = self.firstMove
            
            dispatch_async(dispatch_queue_create("com.myTicTacToe.saveGame", nil), {
        
                MTTDataManager.saveCurrentGame(dbWinner, firstMove: dbFirstMove, firstPlayer: dbFirstPlayer)
                dispatch_async(dispatch_get_main_queue(), {
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(RELOAD_STATISTICS, object: nil)
                })
            })
        }
        
        return gameIsFinished
    }
    
    func arrayContainsVictoriousCombination(array: Array<NSNumber>) -> Bool
    {
        if ((array.contains(zero) && array.contains(one) && array.contains(two)) || (array.contains(three) && array.contains(four) && array.contains(five)) || (array.contains(six) && array.contains(seven) && array.contains(eight)) || (array.contains(zero) && array.contains(three) && array.contains(six)) || (array.contains(one) && array.contains(four) && array.contains(seven)) || (array.contains(two) && array.contains(five) && array.contains(eight)) || (array.contains(zero) && array.contains(four) && array.contains(eight)) || (array.contains(two) && array.contains(four) && array.contains(six)))
        {
            return true
        }
        
        return false
    }
    
    func equality() -> Bool
    {
        var totalArray = Array<NSNumber>()
        
        totalArray.appendContentsOf(player1Array)
        totalArray.appendContentsOf(player2Array)
        
        if (totalArray.contains(zero) && totalArray.contains(one) && totalArray.contains(two) && totalArray.contains(three) && totalArray.contains(four) && totalArray.contains(five) && totalArray.contains(six) && totalArray.contains(seven) && totalArray.contains(eight))
        {
            return true
        }
        return false
    }
    
    func displayEndOfGameMessageAndStartNewGame(title: String, message: String)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            self.loadNewGame()
        }))
        self.presentViewController(alertView, animated: true, completion: {})
    }
    
    func playSound(sound: String)
    {
        if MTTUtils.soundIsEnabled()
        {
            let soundsQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            dispatch_async(soundsQueue, {
                
                AudioSamplePlayer.sharedInstance().playAudioSample(sound, gain: 3.0, pitch: 1.0)
            });
        }
    }
    
    func displayMenu()
    {
        let menuVC = MTTMenuViewController()
        self.navigationController?.showViewController(menuVC, sender: self)
    }
    
    func updateTheme()
    {
        self.backgroundImageView.update()
        self.backgroundImageView.backgroundColor = MTTStyle.getThirdColor()
        self.collectionView?.backgroundColor = MTTStyle.getFourthColor()
        self.changeLabelsColor()
        self.reloadGame()
    }
    
    func changeLabelsColor()
    {
        currentModeLabel?.updateColors()
        scorePlayer1Label?.updateColors()
        scorePlayer2Label?.updateColors()
    }
    
    func updateSounds()
    {
        MTTUtils.activateAVAudioSession()
        self.setUpAudioEffects()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {

        if DEBUG
        {
            if let touch = touches.first
            {
                let location = touch.locationInView(self.view)
                print(location)
            }
            
            let touch = touches.first! as UITouch
            NSLog("touch view : %@", touch.view!)
        }
    }
}
