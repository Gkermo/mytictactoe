//
//  MTTHeaderView.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTHeaderView: UIView {

    let label = UILabel()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = MTTStyle.getSecondColor()
        
        label.textColor = MTTStyle.getFourthColor()
        label.font = UIFont.appFontOfType(.Bold, size: 15)
        if MTTThemesManager.getCurrentTheme() == HELL_THEME
        {
            self.alpha = ELEMENTS_ALPHA
        }
        else
        {
            self.alpha = 1
        }
        
        self.addSubview(label)
        
        label.snp_makeConstraints { (make) -> Void in
            
            make.left.equalTo(self).inset(10)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
