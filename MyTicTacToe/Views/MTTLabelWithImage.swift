//
//  MTTLabelWithImage.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 17/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTLabelWithImage: UIView {

    let imageView = UIImageView()
    let label = MTTLabel()
    
    
    init(text: String, image: UIImage)
    {
        self.label.text = text
        self.imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
        self.imageView.tintColor = MTTStyle.getSecondColor()
        
        super.init(frame: CGRectZero)
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = MTTStyle.getMainColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateLayout()
    {
        self.addSubview(label)
        self.addSubview(imageView)

        label.snp_makeConstraints { (make) -> Void in
            
            make.left.equalTo(imageView.snp_right).inset(-10)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            make.right.equalTo(self)
        }
        
        imageView.snp_makeConstraints { (make) -> Void in
            
            make.left.equalTo(self)
            make.centerY.equalTo(self)
        }
    }
    
    func updateColors()
    {
        self.imageView.tintColor = MTTStyle.getSecondColor()
        self.label.updateColors()
    }
}
