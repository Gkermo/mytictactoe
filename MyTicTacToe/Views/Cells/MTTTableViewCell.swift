//
//  MTTTableViewCell.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.textLabel?.textColor = MTTStyle.getFifthColor()
        self.textLabel?.font = UIFont.appFontOfType(.Normal, size: 14)
        UITableViewCell.appearance().tintColor = MTTStyle.getMainColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update()
    {
        UIView.animateWithDuration(ANIMATION_THEME_CHANGE / 2, animations: {
            if MTTThemesManager.getCurrentTheme() == HELL_THEME
            {
                self.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: Float(ELEMENTS_ALPHA))
            }
            else
            {
                self.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 1)
            }
            
            UITableViewCell.appearance().tintColor = MTTStyle.getMainColor()
        })
    }
}
