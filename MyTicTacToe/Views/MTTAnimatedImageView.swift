//
//  MTTAnimatedImageView.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 10/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import SnapKit

class MTTAnimatedImageView: UIImageView {
    
    override init(image: UIImage?) {
        
        super.init(image: image)
        
        self.animationDuration = 1.75
        self.animationRepeatCount = 0
        self.userInteractionEnabled = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update()
    {
        self.animationImages = nil
        UIView.animateWithDuration(ANIMATION_THEME_CHANGE / 2, animations: {
            self.alpha = 0.0
        })
        if MTTThemesManager.getCurrentTheme() == HELL_THEME
        {
            self.animationImages = MTTUtils.getAnimatedImagesForTheme(HELL_THEME)
            if self.animationImages?.count > 0
            {
                self.startAnimating()
            }
            else
            {
                self.animationImages = nil
            }
        }
        else
        {
            self.animationImages = nil
        }
        UIView.animateWithDuration(ANIMATION_THEME_CHANGE / 2, animations: {
            self.alpha = 1.0
        })
    }
}
