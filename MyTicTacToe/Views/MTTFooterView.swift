//
//  MTTFooterView.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 10/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTFooterView: UIView {

    let label = UILabel()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        
        label.textColor = MTTStyle.getSecondColor()
        label.font = UIFont.appFontOfType(.Bold, size: 15)
        label.textAlignment = .Center
        label.numberOfLines = 0
        
        self.addSubview(label)
        
        let insets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        // The next left and right constraints throws a warning probably linked to this :
        // http://stackoverflow.com/questions/23308400/auto-layout-what-creates-constraints-named-uiview-encapsulated-layout-width-h
        // Setting a lower priority to these constraints ( or to one of them ) avoid the warning to be thrown but doesn't provide the correct behaviour
        label.snp_makeConstraints { (make) -> Void in
            
            make.left.equalTo(self).inset(insets)
            make.top.equalTo(self)
            make.right.equalTo(self).inset(insets)
            make.bottom.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
