//
//  MTTScoreLabel.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTScoreLabel: UILabel {

    override func drawRect(rect: CGRect)
    {
        self.font = UIFont.appFontOfType(.Normal, size: 15)
        self.textColor = MTTStyle.getSecondColor()
        self.backgroundColor = UIColor.clearColor()
    }
}
