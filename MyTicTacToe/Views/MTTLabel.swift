//
//  MTTLabel.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTLabel: UILabel {

    override init(frame:CGRect)
    {
        super.init(frame:frame)
        
        self.font = UIFont.appFontOfType(.Bold, size: 15)
        self.textColor = MTTStyle.getSecondColor()
        self.backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateColors()
    {
        self.textColor = MTTStyle.getSecondColor()
    }
}
