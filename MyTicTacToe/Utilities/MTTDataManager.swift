//
//  MTTDataManager.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 28/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import RealmSwift

class MTTDataManager: NSObject {

    class func saveCurrentGame(result: String, firstMove: NSNumber, firstPlayer: String)
    {
        let game = RLMGame()
        game.mode = MTTUtils.getCurrentMode()
        game.difficulty = MTTUtils.getCurrentDifficulty()
        game.result = result
        game.firstMove = firstMove
        game.firstPlayer = firstPlayer
        
        addObjectToRealm(game)
    }
    
    class func addObjectToRealm(object: Object)
    {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object)
        }
    }
}
