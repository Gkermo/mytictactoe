//
//  MTTUtils.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 03/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import AVFoundation

class MTTUtils: NSObject {

    class func setNormalizedDbValue(value: String) -> String
    {
        var normalizedValue = ""
        if value == NSLocalizedString("player", comment: "")
        {
            normalizedValue = "player"
        }
        else if value == NSLocalizedString("computer", comment: "")
        {
            normalizedValue = "computer"
        }
        else if value == NSLocalizedString("player_1", comment: "")
        {
            normalizedValue = "player_1"
        }
        else if value == NSLocalizedString("player_2", comment: "")
        {
            normalizedValue = "player_2"
        }
        else if value == DRAW
        {
            normalizedValue = "draw"
        }
        
        return normalizedValue
    }
    
    class func removeAllSubviewsFrom(view: UIView)
    {
        let subviews = view.subviews
        for subview in subviews
        {
            subview.removeFromSuperview()
        }
    }
    
    // Mode ( Player VS Player or Player VS Computer )
    class func getCurrentMode() -> String
    {
        let defaults = NSUserDefaults()
        if let currentMode = defaults.stringForKey(KEY_MODE)
        {
            return currentMode
        }
        
        return PLAYER_VS_COMPUTER_MODE
    }
    
    class func setCurrentModeIfNotAlreadySet()
    {
        let defaults = NSUserDefaults()
        if ((defaults.stringForKey(KEY_MODE) == nil))
        {
            defaults.setObject(PLAYER_VS_COMPUTER_MODE, forKey: KEY_MODE)
            defaults.synchronize()
        }
    }
    
    class func setCurrentModeTo(mode: String)
    {
        let defaults = NSUserDefaults()
        if (defaults.stringForKey(KEY_MODE) == mode)
        {
            return
        }
        defaults.setObject(mode, forKey: KEY_MODE)
        defaults.synchronize()
    }
    
    // Difficulty ( Normal or Hard )
    class func getCurrentDifficulty() -> String
    {
        let defaults = NSUserDefaults()
        if let currentDifficulty = defaults.stringForKey(KEY_DIFFICULTY)
        {
            return currentDifficulty
        }
        
        return DIFFICULTY_NORMAL
    }
    
    class func setCurrentDifficultyIfNotAlreadySet()
    {
        let defaults = NSUserDefaults()
        if ((defaults.stringForKey(KEY_DIFFICULTY) == nil))
        {
            defaults.setObject(DIFFICULTY_NORMAL, forKey: KEY_DIFFICULTY)
            defaults.synchronize()
        }
    }
    
    class func setCurrentDifficultyTo(difficulty: String)
    {
        let defaults = NSUserDefaults()
        if (defaults.stringForKey(KEY_DIFFICULTY) == difficulty)
        {
            return
        }
        defaults.setObject(difficulty, forKey: KEY_DIFFICULTY)
        defaults.synchronize()
    }

    // Sound
    class func setSoundEnabledIfNotAlreadySet()
    {
        let defaults = NSUserDefaults()
        if ((defaults.stringForKey(KEY_SOUND_ENABLED) == nil))
        {
            defaults.setObject(SOUND_ON, forKey: KEY_SOUND_ENABLED)
            defaults.synchronize()
        }
    }
    
    class func soundIsEnabled() -> Bool
    {
        let defaults = NSUserDefaults()
        if let soundStatus = defaults.stringForKey(KEY_SOUND_ENABLED)
        {
            return soundStatus == SOUND_ON ? true : false
        }
        
        return false
    }
    
    class func setSoundEnabled(soundEnabled: Bool)
    {
        let defaults = NSUserDefaults()
        if (soundEnabled)
        {
            defaults.setObject(SOUND_ON, forKey: KEY_SOUND_ENABLED)
        }
        else
        {
            defaults.setObject(SOUND_OFF, forKey: KEY_SOUND_ENABLED)
        }
        
        defaults.synchronize()

    }
    
    class func setDefaultsSoundTypeIfNotAlreadySet()
    {
        let defaults = NSUserDefaults()
        if defaults.objectForKey(KEY_SOUND_TYPE) == nil
        {
            defaults.setObject(SOUND_TYPE_MAN, forKey: KEY_SOUND_TYPE)
        }
        
        defaults.synchronize()
    }
    
    class func setSoundType(soundType: String)
    {
        let defaults = NSUserDefaults()
        if let currentSoundType = defaults.objectForKey(KEY_SOUND_TYPE)
        {
            if currentSoundType is String
            {
                if currentSoundType as! String == soundType
                {
                    return
                }
            }
        }
        
        defaults.setObject(soundType, forKey: KEY_SOUND_TYPE)
        defaults.synchronize()
        NSNotificationCenter.defaultCenter().postNotificationName(CHANGE_SOUNDS, object: nil)
    }
    
    class func getCurrentSoundType() -> String
    {
        let defaults = NSUserDefaults()
        if let currentSoundType = defaults.objectForKey(KEY_SOUND_TYPE)
        {
            if currentSoundType is String
            {
                return currentSoundType as! String
            }
        }
        
        return SOUND_TYPE_MAN
    }
    
    class func getSoundIdentifier(soundName: String) -> String
    {
        switch soundName
        {
        case SOUND_LETS_PLAY:
            return NSLocalizedString("sound_lets_play", comment: "") + MTTUtils.getCurrentSoundType()
        case SOUND_DRAW:
            return NSLocalizedString("sound_draw", comment: "") + MTTUtils.getCurrentSoundType()
        case SOUND_LOST:
            return NSLocalizedString("sound_lost", comment: "") + MTTUtils.getCurrentSoundType()
        case SOUND_WIN:
            return NSLocalizedString("sound_win", comment: "") + MTTUtils.getCurrentSoundType()
        default:
            break;
        }
        
        return "pop"
    }
    
    class func activateAVAudioSession()
    {
        AVAudioSession.sharedInstance()
        do
        {
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch let error as NSError
        {
            NSLog("Error AVAudioSession setActive : %@", error)
        }
    }
    
    // Numbers
    class func setToZeroIfNaN(var number: Double)
    {
        if number.isNaN
        {
            number = 0
        }
    }
    
    // Colors storage
    class func colorFromString(string: String) -> UIColor
    {
        let values = string.componentsSeparatedByString(" ")
        switch values.count
        {
        case 3:
            let white = CGFloat((values[1] as NSString).doubleValue)
            let alpha = CGFloat((values[2] as NSString).doubleValue)
            
            return UIColor(white: white, alpha: alpha)
        case 5:
            let red = CGFloat((values[1] as NSString).doubleValue)
            let green = CGFloat((values[2] as NSString).doubleValue)
            let blue = CGFloat((values[3] as NSString).doubleValue)
            let alpha = CGFloat((values[4] as NSString).doubleValue)
            
            return UIColor(red: red, green: green, blue: blue, alpha: alpha)
        default:
            break;
        }
        
        return UIColor.whiteColor()
    }
    
    class func stringFromUIColor(color: UIColor) -> String
    {
        return String(color)
    }
    
    // Other
    class func getAnimatedImagesForTheme(theme: String) -> Array<UIImage>
    {
        var animatedImages = Array<UIImage>()
        var allImagesLoaded = false
        var i = 0
        while !allImagesLoaded
        {
            if let image = UIImage(named: theme + "_background_" + String(i))
            {
                animatedImages.append(image)
                i++
            }
            else
            {
                allImagesLoaded = true
            }
        }
        
        return animatedImages
    }
}
