//
//  MTTMacros.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 25/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import Foundation
import UIKit

// Debug
let DEBUG = false

// UI
let MARGIN : CGFloat = 3
let COLLECTION_VIEW_CELL_ID = "CollectionViewCellIdentifier"
let TABLE_VIEW_CELL_ID = "OptionsTableViewCellIdentifier"
let TABLE_VIEW_CELL_HEADER_ID = "OptionsTableViewCellHeaderIdentifier"

// Game settings
let KEY_MODE = "keyMode"
let KEY_SOUND_ENABLED = "keySoundEnabled"
let PLAYER_VS_COMPUTER_MODE = "player_vs_computer"
let PLAYER_VS_PLAYER_MODE = "player_vs_player"
let KEY_DIFFICULTY = "keyDifficulty"
let DIFFICULTY_NORMAL = "Normal"
let DIFFICULTY_HARD = "Hard"
let SOUND_ON = "soundOn"
let SOUND_OFF = "soundOff"
let KEY_SOUND_TYPE = "keySoundType"
let SOUND_TYPE_MAN = "soundTypeMan"
let SOUND_TYPE_WOMAN = "soundTypeWoman"
let SOUND_LETS_PLAY = "soundLetsPlay"
let SOUND_DRAW = "soundDraw"
let SOUND_LOST = "soundLost"
let SOUND_WIN = "soundWin"

// Game utilities
let zero = NSNumber(integer: 0)
let one = NSNumber(integer: 1)
let two = NSNumber(integer: 2)
let three = NSNumber(integer: 3)
let four = NSNumber(integer: 4)
let five = NSNumber(integer: 5)
let six = NSNumber(integer: 6)
let seven = NSNumber(integer: 7)
let eight = NSNumber(integer: 8)
let DRAW = "Draw"

// Notifications
let RELOAD_STATISTICS = "reloadStatistics"
let CHANGE_THEME = "changeTheme"
let CHANGE_SOUNDS = "changeSounds"

// Controllers
let APP_DELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate
let GAME_VIEW_CONTROLLER = APP_DELEGATE.gameVC

// Style
let KEY_MAIN_COLOR = "keyMainColor"
let KEY_SECOND_COLOR = "keySecondColor"
let KEY_THIRD_COLOR = "keyThirdColor"
let KEY_FOURTH_COLOR = "keyFourthColor"
let KEY_FIFTH_COLOR = "keyFifthColor"
let KEY_THEME = "keyTheme"
let GREEN_THEME = "greenTheme"
let BLUE_THEME = "blueTheme"
let HELL_THEME = "hellTheme"
let ELEMENTS_ALPHA : CGFloat = 0.7
let ANIMATION_THEME_CHANGE = 1.0
// Colors
let cyanColor = UIColor(hex: 0x07A4AF)
let blueColor = UIColor(hex: 0x6380A2)
let lightBlueColor = UIColor(hex: 0xD0D9E3)
let blackColor = UIColor.blackColor()
let redColor = UIColor.redColor()
let yellowColor = UIColor.yellowColor()
let whiteColor = UIColor.whiteColor()
let orangeColor = UIColor(hex: 0xb36b00)
let lightGray = UIColor(hex: 0xbfbfbf)
let darkGreenColor = UIColor(hex: 0x0F571D)
let greenColor = UIColor(hex: 0x8da733)
let lightGreenColor = UIColor(hex: 	0xdaf57b)
// Sizes
let HEIGHT_TABLE_VIEW_HEADER : CGFloat = 25
struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}