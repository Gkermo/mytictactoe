//
//  MTTThemesManager.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 10/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit

enum ThemeIdentifier
{
    case greenThemeId
    case blueThemeId
    case hellThemeId
}

class MTTThemesManager: NSObject {

    class func changeToTheme(identifier: String)
    {
        var theme = ""
        var mainColor = UIColor()
        var secondColor = UIColor()
        var thirdColor = UIColor()
        var fourthColor = UIColor()
        var fifthColor = UIColor()
        
        switch identifier
        {
        case GREEN_THEME:
            
            theme = GREEN_THEME
            mainColor = darkGreenColor
            secondColor = greenColor
            thirdColor = whiteColor
            fourthColor = whiteColor
            fifthColor = blackColor
            
        case BLUE_THEME:
            
            theme = BLUE_THEME
            mainColor = cyanColor
            secondColor = blueColor
            thirdColor = lightBlueColor
            fourthColor = whiteColor
            fifthColor = blackColor
            
        case HELL_THEME:
            
            theme = HELL_THEME
            mainColor = redColor
            secondColor = orangeColor
            thirdColor = lightGray
            fourthColor = blackColor
            fifthColor = blackColor
            
        default:
            break
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(theme, forKey: KEY_THEME)
        defaults.synchronize()
        
        MTTStyle.setMainColor(mainColor)
        MTTStyle.setSecondColor(secondColor)
        MTTStyle.setThirdColor(thirdColor)
        MTTStyle.setFourthColor(fourthColor)
        MTTStyle.setFifthColor(fifthColor)
        
        NSNotificationCenter.defaultCenter().postNotificationName(CHANGE_THEME, object: nil)
    }
    
    class func setDefaultThemeIfNoThemeSet()
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let theme = defaults.objectForKey(KEY_THEME)
        if theme == nil
        {
            self.changeToTheme(GREEN_THEME)
        }
    }
    
    class func getCurrentTheme() -> String
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let theme = defaults.objectForKey(KEY_THEME)
        {
            if theme is String
            {
                return theme as! String
            }
        }
        
        return GREEN_THEME
    }
}
