//
//  MTTUserDefaults.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 09/01/16.
//  Copyright © 2016 Guillaume Kermorgant. All rights reserved.
//

import UIKit

class MTTUserDefaults: NSUserDefaults {

}

extension NSUserDefaults {
    
    func colorForKey(key: String) -> UIColor?
    {
        var color: UIColor?
        if let colorData = dataForKey(key)
        {
            color = NSKeyedUnarchiver.unarchiveObjectWithData(colorData) as? UIColor
        }
        return color
    }
    
    func setColor(color: UIColor?, forKey key: String)
    {
        var colorData: NSData?
        if let color = color
        {
            colorData = NSKeyedArchiver.archivedDataWithRootObject(color)
        }
        setObject(colorData, forKey: key)
    }
}