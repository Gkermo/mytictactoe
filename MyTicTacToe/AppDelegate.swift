//
//  AppDelegate.swift
//  MyTicTacToe
//
//  Created by Guillaume Kermorgant on 24/12/15.
//  Copyright © 2015 Guillaume Kermorgant. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBarVC = MTTTabBarViewController()
    var gameVC = MTTGameViewController()
    var gameNavigationController = MTTNavigationController()
    var statisticsVC = MTTStatisticsViewController()
    var statisticsNavigationController = MTTNavigationController()
    var menuVC = MTTMenuViewController()
    var menuNavigationController = MTTNavigationController()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.backgroundColor = UIColor.whiteColor()
        self.window?.makeKeyAndVisible()
        
        MTTUtils.setCurrentModeIfNotAlreadySet()
        MTTUtils.setCurrentDifficultyIfNotAlreadySet()
        MTTUtils.setSoundEnabledIfNotAlreadySet()
        MTTUtils.setDefaultsSoundTypeIfNotAlreadySet()
        MTTThemesManager.setDefaultThemeIfNoThemeSet()
        
        let gameTabBarItem = UITabBarItem(title: NSLocalizedString("game", comment: ""), image: UIImage(named: "logo_tabbar_50"), tag: 0)
        gameTabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3.0)
        self.gameVC.tabBarItem = gameTabBarItem
        
        let statTabBarItem = UITabBarItem(title: NSLocalizedString("statistics", comment: ""), image: UIImage(named: "statistics_icon"), tag: 0)
        statTabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3.0)
        self.statisticsVC.tabBarItem = statTabBarItem
        
        let menuTabBarItem = UITabBarItem(title: NSLocalizedString("settings", comment: ""), image: UIImage(named: "settings_icon"), tag: 0)
        menuTabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3.0)
        self.menuVC.tabBarItem = menuTabBarItem
    
        self.gameNavigationController = MTTNavigationController(rootViewController: gameVC)
        self.gameNavigationController.setUpColors()
        
        self.statisticsNavigationController = MTTNavigationController(rootViewController: statisticsVC)
        self.statisticsNavigationController.setUpColors()
        
        self.menuNavigationController = MTTNavigationController(rootViewController: menuVC)
        self.menuNavigationController.setUpColors()
        
        self.tabBarVC.viewControllers = [self.gameNavigationController, self.statisticsNavigationController, self.menuNavigationController]
        
        self.window?.rootViewController = self.tabBarVC
        
        if DEBUG
        {
            let realm = try! Realm()
            print("Realm path : " + realm.path)
        }
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

